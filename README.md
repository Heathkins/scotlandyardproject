### What is this repository for? ###

This repository holds the Java components of our first year programming coursework at The University of Bristol.

The two main folders are 'cw-model' and 'cw-ai':

'cw-model' is our implementation of the game Scotland Yard. It is a full working model.
'cw-ai' contains our AIs for both MrX and the Detectives.

The code in this repository was developed by Louis Heath and William Hawkins.